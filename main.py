import pygame
import tkinter
from tkinter import messagebox

# GLOBAL CONSTANTS
# RGB CODES
color_dictionary = {
    "white": (255, 255, 255),
    "black": (0, 0, 0),
    "red": (255, 0, 0),
    "green": (0, 255, 0),
    "blue": (0, 0, 255)
}

# GAME CLASS
class Game:
    def __init__(self, window_width, window_height, window_title, fps):
        pygame.init()  # get pygame ready
        self.window = pygame.display  # this is our handle to the window
        self.surface = self.window.set_mode((window_width, window_height))  # we will draw onto the surface
        self.window.set_caption(window_title)  # setting the window's title
        self.fps = fps  # the number of frames we display per second
        self.clock = pygame.time.Clock()  # we use the clock to wait for a certain period of time each frame
        self.events = pygame.event  # used to handle events such as closing the window
        self.running = True  # the variable indicating whether the program is to continue running
        self.objects = set()  # the set of objects the game consists of

    def run(self):
        while self.running:
            self.check_events()
            self.surface.fill(color_dictionary["white"])
            for object in self.objects:
                if (isinstance(object, MovableRectangle)):
                    object.check_key_press()
                object.draw(self.surface)
            self.window.update()
            self.clock.tick(self.fps)

    def quit(self):
        pygame.quit()

    def add_object(self, object):
        self.objects.add(object)

    def won(self, color):
        tkinter.Tk().wm_withdraw()
        messagebox.showinfo("Game over!", "%s won!" % color)
        self.running = False

    def check_events(self):
        for event in self.events.get():
            if event.type == pygame.QUIT:
                self.running = False

# RECTANGLE CLASS
class Rectangle:
    def __init__(self, x, y, width, height, color):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color

    def draw(self, surface):
        pygame.draw.rect(surface, color_dictionary[self.color], (self.x, self.y, self.width, self.height))


# KEY MOVABLE CLASS
# assumes that the this object has self.x and self.y
class KeyMovable:
    def __init__(self, key_left, key_right, key_up, key_down, speed):
        self.key_left = key_left
        self.key_right = key_right
        self.key_up = key_up
        self.key_down = key_down
        self.speed = speed

    def check_key_press(self):
        keys_pressed = pygame.key.get_pressed()
        # bad code, there must be a way to avoid this much duplication (lambda dictionary?!)
        # checks if the instance is Collidable, it then assumes that there is a reset_position_if_collision method
        if keys_pressed[self.key_left]:
            backup = (self.x, self.y)
            self.x -= self.speed
            if isinstance(self, Collidable):
                self.reset_position_if_collision(backup)
        if keys_pressed[self.key_right]:
            backup = (self.x, self.y)
            self.x += self.speed
            if isinstance(self, Collidable):
                self.reset_position_if_collision(backup)
        if keys_pressed[self.key_up]:
            backup = (self.x, self.y)
            self.y -= self.speed
            if isinstance(self, Collidable):
                self.reset_position_if_collision(backup)
        if keys_pressed[self.key_down]:
            backup = (self.x, self.y)
            self.y += self.speed
            if isinstance(self, Collidable):
                self.reset_position_if_collision(backup)

# COLLIDIBLE CLASS
# assume that the object has self.x, self.y, self.width and self.height
class Collidable:
    def reset_position_if_collision(self, backup):
        (backup_x, backup_y) = backup
        for other in game.objects - {self}:
            if self.collided_with(other):
                self.x = backup_x
                self.y = backup_y
                if (isinstance(other, Winning)):
                    game.won(self.color)
                break

    def collided_with(self, other):
        return not (self.x > other.x + other.width or
                    self.y > other.y + other.height or
                    self.x + self.width < other.x or
                    self.y + self.height < other.y)

# KEY MOVABLE RECTANGLE CLASS
class MovableRectangle(Rectangle, KeyMovable, Collidable):
    def __init__(self, start_x, start_y, width, height, color, key_left, key_right, key_up, key_down, speed):
        Rectangle.__init__(self, start_x, start_y, width, height, color)
        KeyMovable.__init__(self, key_left, key_right, key_up, key_down, speed)
        Collidable.__init__(self)


class Winning:
    pass

class WinningRectangle(Rectangle, Winning):
    def __init__(self, x, y, width, height, color):
        super().__init__(x, y, width, height, color)

# PYGAME INITIALIZATION
game = Game(1200, 800, "Example game", 60)

# ADDING OBJECTS TO THE GAME
game.add_object(Rectangle(0, 0, 10, 800, "black"))
game.add_object(Rectangle(0, 0, 1200, 10, "black"))
game.add_object(Rectangle(0, 790, 1200, 10, "black"))
game.add_object(Rectangle(1190, 0, 10, 800, "black"))
game.add_object(MovableRectangle(20, 360, 30, 30, "red", pygame.K_LEFT, pygame.K_RIGHT, pygame.K_UP, pygame.K_DOWN, 2))
game.add_object(MovableRectangle(20, 410, 30, 30, "blue", pygame.K_a, pygame.K_d, pygame.K_w, pygame.K_s, 2))
game.add_object(WinningRectangle(1140, 375, 50, 50, "green"))

# GAME LOOP
game.run()

# GAME END
game.quit()
